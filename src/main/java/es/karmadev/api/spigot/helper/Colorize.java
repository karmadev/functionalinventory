package es.karmadev.api.spigot.helper;

import org.bukkit.ChatColor;

public class Colorize {

    public static String colorize(final String string) {
        if (string == null)
            return "";

        return ChatColor.translateAlternateColorCodes('&', string);
    }
}
