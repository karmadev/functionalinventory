package es.karmadev.api.spigot.helper;

import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.nio.file.*;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * KarmaAPI path utilities
 */
public class PathUtilities {

    /**
     * Create a path, without any
     * result
     *
     * @param path the path to create
     */
    public static void createPath(final Path path) {
        Path targetPath = path;
        if (!targetPath.isAbsolute()) {
            targetPath = targetPath.toAbsolutePath();
        }

        if (Files.exists(targetPath)) return;

        Path parent = targetPath.getParent();
        createDirectory(parent);

        try {
            Files.createFile(targetPath);
        } catch (IOException ex) {
            throw new RuntimeException(ex);
        }
    }

    /**
     * Build a path as a directory
     *
     * @param path the path
     */
    public static void createDirectory(final Path path) {
        if (Files.exists(path)) return;

        try {
            Files.createDirectories(path);
        } catch (IOException ex) {
            throw new RuntimeException(ex);
        }
    }

    /**
     * Destroy a path
     *
     * @param path the path to destroy
     * @return if the path was able to be destroyed
     */
    public static boolean destroy(final Path path) {
        if (Files.isDirectory(path)) {
            try(Stream<Path> sub = Files.list(path)) {
                for (Path subFile : sub.collect(Collectors.toList())) {
                    if (!destroy(subFile)) return false;
                }

                return Files.deleteIfExists(path);
            } catch (IOException ex) {
                throw new RuntimeException(ex);
            }
        } else {
            try {
                return Files.deleteIfExists(path);
            } catch (IOException ex) {
                throw new RuntimeException(ex);
            }
        }
    }

    /**
     * Read the path data
     *
     * @param path the path content
     * @return the path contents
     */
    public static byte[] readBytes(final Path path) {
        if (Files.isDirectory(path) || !Files.exists(path)) return new byte[0];
        try {
            return Files.readAllBytes(path);
        } catch (IOException ex) {
            throw new RuntimeException(ex);
        }
    }

    /**
     * Read the file
     *
     * @param path the file
     * @return the file content
     */
    public static String read(final Path path) {
        return read(path, StandardCharsets.UTF_8);
    }

    /**
     * Read the file
     *
     * @param path the file
     * @param charset the charset to get data as
     * @return the file content
     */
    public static String read(final Path path, final Charset charset) {
        return new String(readBytes(path), charset);
    }

    /**
     * Write to the file
     *
     * @param path the file to write to
     * @param data the file data
     * @param options the write options
     * @return if the file could be written
     */
    public static boolean write(final Path path, final byte[] data, OpenOption... options) {
        if (Files.isDirectory(path)) return false;
        createPath(path);

        try {
            Files.write(path, data, options);
            return true;
        } catch (IOException ex) {
            throw new RuntimeException(ex);
        }
    }

    /**
     * Write to the file
     *
     * @param path the file to write to
     * @param data the file data
     * @param options the write options
     * @return if the file could be written
     */
    public static boolean write(final Path path, final CharSequence data, final OpenOption... options) {
        return write(path, data, StandardCharsets.UTF_8, options);
    }

    /**
     * Write to the file
     *
     * @param path the file to write to
     * @param data the file data
     * @param charset the data charset
     * @param options the write options
     * @return if the file could be written
     */
    public static boolean write(final Path path, final CharSequence data, final Charset charset, final OpenOption... options) {
        return write(path, data.toString().getBytes(charset), options);
    }
}
