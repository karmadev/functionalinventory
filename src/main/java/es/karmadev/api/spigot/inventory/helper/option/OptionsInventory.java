package es.karmadev.api.spigot.inventory.helper.option;

import es.karmadev.api.spigot.helper.Colorize;
import es.karmadev.api.spigot.inventory.helper.func.ItemFunction;
import es.karmadev.api.spigot.inventory.helper.option.func.OptionItemFunction;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.entity.HumanEntity;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.*;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.InventoryHolder;
import org.bukkit.inventory.ItemStack;
import org.bukkit.plugin.Plugin;
import org.jetbrains.annotations.NotNull;

import java.util.Map;
import java.util.Objects;
import java.util.Set;
import java.util.UUID;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;

/**
 * Represents an inventory for a user
 * to make a choice
 */
public class OptionsInventory<T> implements InventoryHolder, Listener {

    protected final ConcurrentMap<Integer, T> choices = new ConcurrentHashMap<>();
    protected final Map<Integer, ItemFunction<OptionsInventory<T>>> functions = new ConcurrentHashMap<>();
    protected final Set<UUID> open = ConcurrentHashMap.newKeySet();
    protected final Inventory inventory;

    protected boolean canClose = false;
    protected boolean closeOnClick = true;

    /**
     * Create an options inventory
     *
     * @param plugin the plugin owning the options
     *               inventory
     * @param title the options inventory title
     * @param size the inventory size
     */
    public OptionsInventory(final Plugin plugin, final String title, final int size) {
        inventory = Bukkit.createInventory(this, size, Colorize.colorize(title));

        Bukkit.getPluginManager().registerEvent(InventoryClickEvent.class, this, EventPriority.HIGHEST, (listener, event) -> {
            assert event instanceof InventoryClickEvent;
            InventoryClickEvent click = (InventoryClickEvent) event;

            if (click.getClickedInventory() != null) {
                if (click.getClickedInventory().getHolder() == this) {
                    click.setCancelled(true);

                    ItemFunction<OptionsInventory<T>> function = functions.getOrDefault(click.getSlot(), null);
                    if (function != null) {
                        function.triggerClick(click);
                        if (closeOnClick) {
                            HumanEntity human = click.getWhoClicked();
                            open.remove(human.getUniqueId());
                            Bukkit.getScheduler().runTaskLater(plugin, human::closeInventory, 10);
                        }
                    }
                }
            }
        }, plugin, false);

        Bukkit.getPluginManager().registerEvent(InventoryMoveItemEvent.class, this, EventPriority.HIGHEST, (listener, event) -> {
            assert event instanceof InventoryMoveItemEvent;
            InventoryMoveItemEvent e = (InventoryMoveItemEvent) event;

            e.setCancelled(e.getDestination().getHolder() == this || e.getInitiator().getHolder() == this);
        }, plugin, true);

        Bukkit.getPluginManager().registerEvent(InventoryDragEvent.class, this, EventPriority.HIGHEST, (listener, event) -> {
            assert event instanceof InventoryDragEvent;
            InventoryDragEvent e = (InventoryDragEvent) event;

            e.setCancelled(e.getInventory().getHolder() == this);
        }, plugin, true);

        Bukkit.getPluginManager().registerEvent(InventoryCloseEvent.class, this, EventPriority.HIGHEST, (listener, event) -> {
            assert event instanceof InventoryCloseEvent;
            InventoryCloseEvent close = (InventoryCloseEvent) event;

            if (close.getInventory().getHolder() == this) {
                HumanEntity human = close.getPlayer();

                if (!canClose && open.contains(human.getUniqueId())) {
                    Bukkit.getScheduler().runTaskLater(plugin, () -> {
                        try {
                            human.openInventory(inventory);
                        } catch (Throwable ignored) {}
                    }, 10);
                }

                if (canClose) open.remove(human.getUniqueId());
            }
        }, plugin, false);
    }

    /**
     * Set if the inventory gets close when
     * an option is clicked
     *
     * @param status the on click close status
     * @return the inventory
     */
    public OptionsInventory<T> setCloseOnClick(final boolean status) {
        this.closeOnClick = status;
        return this;
    }

    /**
     * Set if the inventory can be closed without
     * making a choice
     *
     * @param status the inventory close status
     * @return the inventory
     */
    public OptionsInventory<T> setCanClose(final boolean status) {
        this.canClose = status;
        return this;
    }

    /**
     * Add a choice to the inventory
     *
     * @param element the choice
     * @param slot the choice slot
     * @param display the choice item display
     * @return the choice
     */
    public final ItemFunction<OptionsInventory<T>> addChoice(final T element, final int slot, final ItemStack display) {
        ItemFunction<OptionsInventory<T>> function = new OptionItemFunction<>(this);

        if (slot >= 54 || slot < 0) {
            throw new IllegalArgumentException("Cannot set item to index " + slot + ". Only from 0 to 54");
        }

        inventory.setItem(slot, display);
        choices.put(slot, element);
        functions.put(slot, function);

        return function;
    }

    /**
     * Set the item at index
     *
     * @param slot the index
     * @param display the item
     * @return the choice
     */
    public final ItemFunction<OptionsInventory<T>> setItem(final int slot, final ItemStack display) {
        Objects.requireNonNull(display, "Cannot set null item for inventory");

        if (slot >= 54 || slot < 0) {
            throw new IllegalArgumentException("Cannot set item to index " + slot + ". Only from 0 to 54");
        }

        inventory.setItem(slot, display);
        return functions.computeIfAbsent(slot, (f) -> new OptionItemFunction<>(this));
    }

    /**
     * Update an option item display
     *
     * @param option the option
     * @param display the display override
     * @return the choice
     */
    public final ItemFunction<OptionsInventory<T>> updateChoice(final T option, final ItemStack display) {
        Objects.requireNonNull(display, "Cannot set null item for inventory");

        int slot = getIndex(option);
        inventory.setItem(slot, display);

        return functions.get(slot);
    }

    /**
     * Remove a choice from the inventory
     *
     * @param slot the choice slot
     */
    public final void removeChoice(final int slot) {
        inventory.setItem(slot, null);
        choices.remove(slot);
        functions.remove(slot);
    }

    /**
     * Get the index of the item in the
     * inventory of an option
     *
     * @param option the option
     * @return the option item index
     */
    public final int getIndex(final T option) {
        int slot = -1;
        for (int index : choices.keySet()) {
            T value = choices.get(index);
            if (Objects.equals(value, option)) {
                slot = index;
                break;
            }
        }

        return slot;
    }

    /**
     * Get the object's inventory.
     *
     * @return The inventory.
     */
    @NotNull
    @Override
    public final Inventory getInventory() {
        return inventory;
    }

    /**
     * Close the inventory for the entity
     *
     * @param entity the entity to close the inventory
     *               for
     */
    public void close(final HumanEntity entity) {
        if (open.contains(entity.getUniqueId())) {
            open.remove(entity.getUniqueId());
            entity.closeInventory();
        }
    }

    /**
     * Open the inventory to the entity
     *
     * @param entity the entity to open the
     *               inventory to
     */
    public void open(final HumanEntity entity) {
        if (!open.contains(entity.getUniqueId())) {
            open.add(entity.getUniqueId());
            entity.openInventory(inventory);
        }
    }
}
