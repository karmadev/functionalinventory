package es.karmadev.api.spigot.inventory.helper.infinity.func;

import es.karmadev.api.spigot.inventory.helper.func.Action;
import es.karmadev.api.spigot.inventory.helper.func.GenericAction;
import es.karmadev.api.spigot.inventory.helper.infinity.InventoryBook;
import es.karmadev.api.spigot.inventory.helper.infinity.InventoryPage;
import org.bukkit.Bukkit;
import org.bukkit.plugin.Plugin;

public final class BookAction extends GenericAction<InventoryBook> {

    private BookAction(final Plugin plugin) {
        super(plugin);
    }

    /**
     * Close the inventory
     *
     * @return the action
     */
    public Action<InventoryBook> close() {
        return close(10);
    }

    /**
     * Change the page
     *
     * @param page the new page
     * @return the action
     */
    public Action<InventoryBook> changePage(final int page) {
        return changePage(page, 10);
    }

    /**
     * Go to the next page
     *
     * @return the action
     */
    public Action<InventoryBook> nextPage() {
        return nextPage(10);
    }

    /**
     * Go to the previous page
     *
     * @return the action
     */
    public Action<InventoryBook> previousPage() {
        return previousPage(10);
    }

    /**
     * Close the inventory
     *
     * @param delay the ticks to wait before executing this action
     * @return the action
     */
    public Action<InventoryBook> close(final long delay) {
        return (book, e, player) -> Bukkit.getScheduler().runTaskLater(this.plugin, () -> {
            book.allowClose(player);
            player.closeInventory();
            book.denyClose(player);
        }, delay);
    }

    /**
     * Change the page
     *
     * @param page the new page
     * @param delay the ticks to wait before executing this action
     * @return the action
     */
    public Action<InventoryBook> changePage(final int page, final long delay) {
        return (book, e, player) -> Bukkit.getScheduler().runTaskLater(this.plugin, () -> {
            if (book.getPageIndex(player) != page) {
                InventoryPage new_page = book.getPage(page);
                if (new_page != null) {
                    try {
                        book.allowClose(player);
                        book.open(player, page);
                        book.denyClose(player);
                    } catch (Throwable ignored) {}
                }
            }
        }, delay);
    }

    /**
     * Go to the next page
     *
     * @param delay the ticks to wait before executing this action
     * @return the action
     */
    public Action<InventoryBook> nextPage(long delay) {
        return (book, e, player) -> Bukkit.getScheduler().runTaskLater(this.plugin, () -> {
            int next = book.getNextPageIndex(player);
            if (next != -1) {
                try {
                    book.allowClose(player);
                    book.open(player, next);
                    book.denyClose(player);
                } catch (Throwable ignored) {}
            }
        }, delay);
    }

    /**
     * Go to the previous page
     *
     * @param delay the ticks to wait before executing this action
     * @return the action
     */
    public Action<InventoryBook> previousPage(long delay) {
        return (book, e, player) -> Bukkit.getScheduler().runTaskLater(this.plugin, () -> {
            int previous = book.getPreviousPageIndex(player);
            if (previous != -1) {
                try {
                    book.allowClose(player);
                    book.open(player, previous);
                    book.denyClose(player);
                } catch (Throwable ignored) {}
            }
        }, delay);
    }

    /**
     * Create a new book action
     *
     * @param plugin the plugin owning the
     *               action
     * @return the action
     */
    public static BookAction newAction(final Plugin plugin) {
        return new BookAction(plugin);
    }
}
