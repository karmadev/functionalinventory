package es.karmadev.api.spigot.inventory;

import es.karmadev.api.kson.JsonArray;
import es.karmadev.api.kson.JsonInstance;
import es.karmadev.api.kson.JsonNative;
import es.karmadev.api.kson.JsonObject;
import es.karmadev.api.kson.io.JsonReader;
import es.karmadev.api.spigot.helper.Colorize;
import es.karmadev.api.spigot.helper.PathUtilities;
import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.InventoryView;
import org.bukkit.inventory.ItemStack;
import org.bukkit.plugin.Plugin;
import org.jetbrains.annotations.Nullable;

import java.io.*;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.Base64;
import java.util.Map;
import java.util.UUID;

/**
 * Inventory serializer
 */
public class InventorySerializer {

    private final Plugin plugin;
    private final UUID id;
    private final String title;
    private final transient Inventory inventory;

    /**
     * Initialize the inventory serializer
     *
     * @param plugin the plugin owning the
     *               inventory
     * @param uniqueId the inventory identifier
     * @param title the inventory title
     * @param inventory the inventory to store
     */
    public InventorySerializer(final Plugin plugin, final UUID uniqueId, final String title, final Inventory inventory) {
        this.plugin = plugin;
        this.id = uniqueId;
        this.title = title;
        this.inventory = inventory;
    }

    /**
     * Get the inventory unique ID
     *
     * @return the inventory unique ID
     */
    public UUID getUniqueId() {
        return id;
    }

    /**
     * Get if the data exists
     *
     * @return if the data exists
     */
    public boolean exists() {
        Path pluginWorkingDirectory = this.plugin.getDataFolder().toPath();

        String simple = id.toString().replaceAll("-", "");
        Path data = pluginWorkingDirectory.resolve(simple).resolve("data.json");

        return Files.exists(data);
    }

    /**
     * Saves the inventory data
     * @return if the data could be saved
     */
    public boolean save() {
        Path pluginWorkingDirectory = this.plugin.getDataFolder().toPath();

        String simple = id.toString().replaceAll("-", "");
        Path data = pluginWorkingDirectory.resolve(simple).resolve("data.json");

        if (Files.exists(data)) {
            JsonInstance element = JsonReader.read(PathUtilities.read(data));
            if (element.isObjectType()) return false; //Already exists
        }

        JsonObject object = JsonObject.newObject("", "");
        object.put("title", title.replaceAll("§", "&"));
        object.put("size", inventory.getSize());

        JsonArray slots = JsonArray.newArray("", "slots");
        for (int slot = 0; slot < inventory.getSize(); slot++) {
            ItemStack item = inventory.getItem(slot);
            if (item != null && !item.getType().equals(Material.AIR)) {
                Map<String, Object> itemData = item.serialize();

                JsonObject slotObject = JsonObject.newObject("", "");
                slotObject.put("slot", slot);
                slotObject.put("data", serializeUnsafe(itemData));

                slots.add(slotObject);
            }
        }

        object.put("slots", slots);
        PathUtilities.createPath(data);

        String raw = object.toString();
        return PathUtilities.write(data, raw);
    }

    /**
     * Destroy the stored data if any
     */
    public boolean destroy() {
        Path pluginWorkingDirectory = this.plugin.getDataFolder().toPath();

        String simple = id.toString().replaceAll("-", "");
        Path data = pluginWorkingDirectory.resolve(simple).resolve("data.json");

        return PathUtilities.destroy(data);
    }

    /**
     * Create an inventory serializer for the
     * inventory
     *
     * @param plugin the plugin owning the inventory
     * @param inventory the inventory view
     * @return the inventory serializer
     */
    public static InventorySerializer forInventory(final Plugin plugin, final InventoryView inventory) {
        return new InventorySerializer(plugin, UUID.randomUUID(), inventory.getTitle(), inventory.getTopInventory());
    }

    /**
     * Load an inventory
     *
     * @param plugin the plugin owning the inventory
     * @param inventoryId the inventory ID
     * @return the loaded inventory
     */
    @Nullable
    public static InventorySerializer load(final Plugin plugin, final UUID inventoryId) {
        Path pluginWorkingDirectory = plugin.getDataFolder().toPath();
        String simple = inventoryId.toString().replaceAll("-", "");

        Path data = pluginWorkingDirectory.resolve(simple).resolve("data.json");
        if (!Files.exists(data)) return null;

        JsonInstance element = JsonReader.read(PathUtilities.read(data));
        if (!element.isObjectType()) return null;

        JsonObject object = element.asObject();
        if (!object.hasChild("title") || !object.getChild("title").isNativeType()) return null;

        JsonNative titleData = object.getChild("title").asNative();
        if (!titleData.isString()) return null;

        String title = titleData.getAsString();
        if (title == null)
            title = "";

        if (!object.hasChild("size") || !object.getChild("size").isNativeType()) return null;

        JsonNative sizeData = object.getChild("size").asNative();
        if (!sizeData.isNumber()) return null;

        int size = sizeData.asInteger();

        Inventory inventory = Bukkit.createInventory(null, size, Colorize.colorize(title));

        if (!object.hasChild("slots") || !object.getChild("slots").isArrayType()) return null;
        JsonArray slotsData = object.getChild("slots").asArray();


        for (JsonInstance slotData : slotsData) {
            if (!slotData.isObjectType()) continue;

            JsonObject slotObject = slotData.asObject();
            if (!slotObject.hasChild("slot") || !slotObject.getChild("slot").isNativeType()) continue;

            JsonNative slotPrimitive = slotObject.getChild("slot").asNative();
            if (!slotPrimitive.isNumber()) continue;

            int slotIndex = slotPrimitive.asInteger();

            if (!slotObject.hasChild("data") || !slotObject.getChild("data").isNativeType()) continue;

            JsonNative itemData = slotObject.getChild("data").asNative();
            if (!itemData.isString()) continue;

            Map<String, Object> itemMap = loadAndCast(itemData.getAsString());
            if (itemMap != null) {
                ItemStack stack = ItemStack.deserialize(itemMap);
                ItemStack existing = inventory.getItem(slotIndex);

                if (existing != null && !existing.getType().equals(Material.AIR)) {
                    if (existing.isSimilar(stack)) {
                        existing.setAmount(existing.getAmount() + 1);
                    }

                    inventory.setItem(slotIndex, existing);
                    continue;
                }

                inventory.setItem(slotIndex, stack);
            }
        }

        return new InventorySerializer(plugin, inventoryId, title, inventory);
    }

    /**
     * Serialize an object without knowing its
     * type
     *
     * @param instance the object instance
     * @return the serialized instance
     */
    public static String serializeUnsafe(final Object instance) {
        try(ByteArrayOutputStream bos = new ByteArrayOutputStream(); ObjectOutputStream ous = new ObjectOutputStream(bos)) {
            ous.writeObject(instance);
            ous.flush();

            return Base64.getEncoder().encodeToString(bos.toByteArray());
        } catch (Throwable ex) {
            throw new RuntimeException(ex);
        }
    }

    /**
     * Load a serialized object
     *
     * @param instance the string instance
     * @param <T> the object type
     * @return the resolved object
     */
    @SuppressWarnings("unchecked")
    private static <T> @Nullable T loadAndCast(final String instance) {
        if (instance == null) return null;
        T loaded;

        try {
            byte[] bytes = Base64.getDecoder().decode(instance);
            try (ByteArrayInputStream bis = new ByteArrayInputStream(bytes); ObjectInputStream ois = new ObjectInputStream(bis)) {
                loaded = (T) ois.readObject();
            } catch (IOException | ClassNotFoundException | ClassCastException ex) {
                throw new RuntimeException(ex);
            }
        } catch (IllegalArgumentException ex) {
            throw new RuntimeException(ex);
        }

        return loaded;
    }
}
